## code to prepare `my_dataset` dataset goes here

usethis::use_data(my_dataset, overwrite = TRUE)

library(dplyr)
library(ggplot2)
my_dataset <- slice_sample(diamonds, prop = .2)

usethis::use_data(my_dataset, overwrite = TRUE)
