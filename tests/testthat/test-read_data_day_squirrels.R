# WARNING - Generated by {fusen} from dev/flat_read_multiple_data.Rmd: do not edit by hand

test_that("read_data_day_squirrels works", {

  ipath <- system.file("nyc_squirrels_17.xlsx", 
                       package = "squirrelsrevision") |> dirname()
    
  ldata <- read_data_day_squirrels(input_folderpath = ipath)
  
  expect_true(inherits(ldata, "list"))
  
})
